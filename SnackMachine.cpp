/*
 * SnackMachine.cpp
 *
 *  Created on: 01.05.2017
 *      Author: Magda
 */

#include "SnackMachine.h"
#include <iostream>
#include <iomanip>
using namespace std;


//constructors
SnackMachine::SnackMachine(){
	this->nbrOfProducts=0;
	this->totalPrice=0;
}

//destructors
SnackMachine::~SnackMachine() {
}

//methods
void SnackMachine::addProductToMachine(Product *newProduct){
	if (nbrOfProducts<maxNumberOfProducts){
		productOffer[nbrOfProducts]=newProduct;
		nbrOfProducts++;
	}
}

void SnackMachine::showMenu(){
	for (int i=0;i<nbrOfProducts;i++){
		cout<<i+1<<". "<<productOffer[i]->productName<<"\t"<<"price: "<<productOffer[i]->productPrice<<endl;
	}
}

double SnackMachine::selectProduct(int nbrSelectingProduct){//if client can buy only one product in our machine
	if (nbrSelectingProduct-1<nbrOfProducts){
		totalPrice=productOffer[nbrSelectingProduct-1]->productPrice;
		cout<<" PRICE: "<<totalPrice<<endl;
	}
	else
		cout<<"WRONG NUMBER. PLEASE SELECT PRODUCT NUMBER AGAIN"<<endl;
}

double SnackMachine::payForProduct(int nbrSelectingProduct, double userPayment){//if client can buy only one product in our machine

	if (userPayment==totalPrice){
		cout<<"ORDER COMPLETE.PLEASE TAKE YOUR PRODUCT"<<endl;
	}
	else if (userPayment>totalPrice){
		cout<<">>>PLEASE WAIT>>>GIVING THE CHANGE>>>"<< userPayment-totalPrice<<endl;
	}
	else if (userPayment<totalPrice){
		double restToPay;
		restToPay=totalPrice-userPayment;
		do {
			cout<<fixed;
			cout<<">>>LEFT TO PAY: "<<setprecision(2)<<restToPay<<",COIN IN"<<endl;
			cin>>userPayment;
			restToPay-=userPayment;
		}
		while(restToPay>0);
		if (restToPay==0){
			cout<<">>>ORDER COMPLETE.PLEASE TAKE YOUR PRODUCT<<<"<<endl;
		}
		else
			cout<<">>>ORDER COMPLETE.PLEASE TAKE YOUR CHANGE: "<<0-restToPay<<" AND PRODUCT<<<<"<<endl;
	}
}

double SnackMachine::selectProducts(){ //if client can buy more than one product in our machine
	bool moreProducts;
	int productNumber;
	do {
	cout<<"PLEASE SELECT PRODUCT NUMBER FROM THE LIST:"<<endl;
	showMenu();
	cin>>productNumber;
		if(productNumber>0 || productNumber<nbrOfProducts){
			totalPrice+=productOffer[productNumber-1]->productPrice;
			cout<<"TOTAL PRICE: "<<totalPrice<<endl;
			cout<<"DO YOU WANT TO BUY MORE PRODUCTS? 0-NO, 1-YES"<<endl;
			cin>>moreProducts;
		}
		else
			cout<<"WRONG NUMBER. PLEASE SELECT PRODUCT NUMBER AGAIN"<<endl;
	}
	while (moreProducts!=false);
	cout<<"TOTAL PRICE: "<<totalPrice<<endl;
}




