/*
 * Product.h
 *
 *  Created on: 29.04.2017
 *      Author: Magda
 */

#ifndef PRODUCT_H_
#define PRODUCT_H_
#include <iostream>
using namespace std;


class Product {
public:
//constructors
	Product();
	Product(string productName, double price);

//destructors
	~Product();

//arguments
	string productName;
	double productPrice;

};

#endif /* PRODUCT_H_ */
