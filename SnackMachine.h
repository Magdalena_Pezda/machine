/*
 * SnackMachine.h
 *
 *  Created on: 01.05.2017
 *      Author: Magda
 */

#ifndef SNACKMACHINE_H_
#define SNACKMACHINE_H_
#include "Product.h"

const int maxNumberOfProducts=30;

class SnackMachine: public Product{
public:
//constructor
	SnackMachine();
//destructors
	virtual ~SnackMachine();
//methods
	void addProductToMachine(Product *newProduct);
	void showMenu();
	double selectProduct(int nbrSelectingProduct); //if client want to buy only one product in our machine
	double selectProducts(); //if client want to buy more than one product in our machine
	double payForProduct(int nbrSelectingProduct,double userPayment); //if client want to buy only one product in our machine


//atributes
	Product *productOffer[maxNumberOfProducts];
	int nbrOfProducts;
	Product newProduct;
	double totalPrice;

};

#endif /* SNACKMACHINE_H_ */

//
//kupno produktu w automacie:
//	pobieramy dostępny asortyment automatu + ceny
//	wybieramy produkt
//	wrzucamy odliczoną kwotę
//	automat wydaje produkt
//
//
//rozszerzenia:
//	automat ma limitowaną ilość produktu OK
//	automat ma limit ilości monet jaką jest w stanie przyjąć
//	można kupić więcej produktów OK
//	można załadować automat nowym asortymentem
//	można wybrać z niego monety
